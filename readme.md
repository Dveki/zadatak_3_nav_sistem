Kreirati web stranicu koja poseduje navigacioni sistem
primenom listi, formu, slike i tekst. Struktura web sajta da se
odradi primenom div elemenata i float atributa. Stilizovati
elemente forme primenom odgovarajućih CSS selektora i
CSS klasa. Naglasak treba da bude na pravilnoj upotrebi
CSS selektora i CSS klasa kako bi se dobio optimizovani
CSS kod. Obavezno korišćenje spoljašnjeg CSS fajla.